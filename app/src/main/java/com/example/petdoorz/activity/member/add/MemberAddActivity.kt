package com.example.petdoorz.activity.member.add

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.view.*
import com.example.petdoorz.R
import com.google.gson.JsonObject
import com.google.gson.JsonArray
import android.widget.*
import com.example.petdoorz.databinding.ActivityMemberAddBinding
import com.example.petdoorz.helpers.HelpersData

class MemberAddActivity : AppCompatActivity(), MemberAddView {
    private var binding: ActivityMemberAddBinding? = null
    private var presenter: MemberAddPresenter? = null
    private var iType: String? = null
    private var iId: Int? = 0
    private var iName: String? = null
    private var iEmail: String? = null
    private var edtName: EditText? = null
    private var edtEmail: EditText? = null
    private var edtPassword: EditText? = null
    private var btnSave: Button? = null

    private var helpersData: HelpersData? = null
    private var alertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMemberAddBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)

        edtName = binding!!.edtName
        edtEmail = binding!!.edtEmail
        edtPassword = binding!!.edtPassword
        btnSave = binding!!.btnSave

        intentFromActivity

        helpersData = HelpersData(this)

        presenter = MemberAddPresenter(this)
        if (iType == "add") {
            supportActionBar!!.title = "ADD"
        } else {
            supportActionBar!!.title = "EDIT"
        }

        btnSave!!.setOnClickListener {
            val jsonObject = JsonObject()
            jsonObject.addProperty("name", edtName!!.text.toString().trim { it <= ' ' })
            jsonObject.addProperty("email", edtEmail!!.text.toString().trim { it <= ' ' })
            jsonObject.addProperty("password", edtPassword!!.text.toString().trim { it <= ' ' })
            if (iType == "add") {
                presenter!!.createMember(jsonObject)
            } else if (iType == "edit") {
                presenter!!.updateMember(iId!!, jsonObject)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showLoading() {
        alertDialog = helpersData!!.progressDialog()
        alertDialog?.show()
    }

    override fun hideLoading() {
        alertDialog?.dismiss()
    }

    override fun messages(messages: JsonArray?) {
        val element = messages?.get(0)
        val obj = element?.asJsonObject
        val tErrorName: TextView = binding!!.tvErrorName
        val tErrorEmail: TextView = binding!!.tvErrorEmail
        val tErrorPassword: TextView = binding!!.tvErrorPassword
        tErrorName.visibility = View.GONE
        tErrorEmail.visibility = View.GONE
        tErrorPassword.visibility = View.GONE

        if(obj?.isJsonNull == false) {
            for (key in obj.keySet()) {
                val message = obj[key].asJsonArray
                if(key.equals("name")) {
                    tErrorName.visibility = View.VISIBLE
                    tErrorName.text = message?.get(0)?.asString
                }

                if(key.equals("email")) {
                    tErrorEmail.visibility = View.VISIBLE
                    tErrorEmail.text = message?.get(0)?.asString
                }

                if(key.equals("password")) {
                    tErrorPassword.visibility = View.VISIBLE
                    tErrorPassword.text = message?.get(0)?.asString
                }
            }
        }
    }

    override fun success(success: Boolean?) {
        if (success!!) {
            val returnIntent = Intent()
            returnIntent.putExtra("type", iType)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

    // method for get the data from home activity
    private val intentFromActivity: Unit
    get() {
        val intent = intent
        iType = intent.getStringExtra("type")
        iId = intent.getIntExtra("id", 0)
        iName = intent.getStringExtra("name")
        iEmail = intent.getStringExtra("email")

        edtName!!.setText(iName)
        edtEmail!!.setText(iEmail)
    }

    companion object {
        private val tag = MemberAddActivity::class.java.simpleName
    }
}