package com.example.petdoorz.activity.login

import android.util.Log
import com.example.petdoorz.api.ApiClient.apiClient
import com.google.gson.JsonObject
import com.example.petdoorz.api.ApiInterface
import com.example.petdoorz.model.login.LoginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter     // interface constructor
    (private val view: LoginView) {
    fun login(body: JsonObject?) {
        Log.d(tag, "getData")
        view.showLoading()

        //request to server
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.login(body)
        call!!.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(
                call: Call<LoginResponse?>,
                response: Response<LoginResponse?>
            ) {
                Log.d(tag, "onResponse")
                view.hideLoading()
                println(response)
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.onResult(response.body()?.data)
                    } else {
                        Log.d(tag, "isSuccess false")
                        view.errors(response.body()?.message)
                    }
                } else {
                    Log.d(tag, "body empty")
                    println(response)
                }
            }

            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                Log.d(tag, "onFailure")
                Log.d(tag, t.localizedMessage)
                view.hideLoading()
            }
        })
    }

    companion object {
        private val tag = LoginPresenter::class.java.simpleName
    }
}