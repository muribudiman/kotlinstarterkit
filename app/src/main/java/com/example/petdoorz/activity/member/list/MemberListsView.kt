package com.example.petdoorz.activity.member.list

import com.example.petdoorz.model.member.MemberData
import com.example.petdoorz.model.pagination.PaginationData

interface MemberListsView {
    fun showItemLoading()
    fun hideItemLoading()
    fun onResult(data: List<MemberData?>?, pagination: PaginationData?)
}