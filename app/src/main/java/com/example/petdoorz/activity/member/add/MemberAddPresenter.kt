package com.example.petdoorz.activity.member.add

import android.util.Log
import com.example.petdoorz.activity.login.LoginPresenter
import com.example.petdoorz.api.ApiClient.apiClient
import com.google.gson.JsonObject
import com.example.petdoorz.api.ApiInterface
import com.example.petdoorz.model.general.GeneralResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MemberAddPresenter(private val view: MemberAddView) {
    fun createMember(body: JsonObject?) {
        view.showLoading()
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.postMember(body)
        call!!.enqueue(object : Callback<GeneralResponse?> {
            override fun onResponse(
                call: Call<GeneralResponse?>,
                response: Response<GeneralResponse?>
            ) {
                view.hideLoading()
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.success(true)
                    } else {
                        Log.d(tag, "isSuccess false")
                        view.messages(response.body()!!.message)
                    }
                } else if (response.code() == 401) {
                    Log.d(tag, "401")
                } else {
                    Log.d(tag, "body empty")
                }
            }

            override fun onFailure(call: Call<GeneralResponse?>, t: Throwable) {
                //Log.d(tag, "onFailure");
                //Log.d(tag, t.getLocalizedMessage());
                view.hideLoading()
            }
        })
    }

    fun updateMember(id: Int, body: JsonObject?) {
        view.showLoading()
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.putMember(id, body)
        call!!.enqueue(object : Callback<GeneralResponse?> {
            override fun onResponse(
                call: Call<GeneralResponse?>,
                response: Response<GeneralResponse?>
            ) {
                view.hideLoading()
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.success(true)
                    } else {
                        Log.d(tag, "isSuccess false")
                        view.messages(response.body()!!.message)
                    }
                } else if (response.code() == 401) {
                    Log.d(tag, "401")
                } else {
                    Log.d(tag, "body empty")
                }
            }

            override fun onFailure(call: Call<GeneralResponse?>, t: Throwable) {
                //Log.d(tag, "onFailure");
                //Log.d(tag, t.getLocalizedMessage());
                view.hideLoading()
            }
        })
    }

    companion object {
        private val tag = MemberAddPresenter::class.java.simpleName
    }
}