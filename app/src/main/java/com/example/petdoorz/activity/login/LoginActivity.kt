package com.example.petdoorz.activity.login

import com.example.petdoorz.model.login.LoginData
import android.os.Bundle
import android.content.pm.ActivityInfo
import com.example.petdoorz.helpers.SharedPrefManager
import android.content.Intent
import com.example.petdoorz.MainActivity
import com.google.gson.JsonObject
import android.os.Build
import android.view.View
import android.widget.TextView
import com.google.gson.JsonArray
import androidx.appcompat.app.AppCompatActivity
import com.example.petdoorz.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity(), LoginView {
    private var binding: ActivityLoginBinding? = null
    private var logins: Array<LoginData>? = null
    private var presenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        presenter = LoginPresenter(this)
        val login = binding!!.btnLogin
        if (SharedPrefManager.getInstance(this@LoginActivity)!!.isLoggedIn) {
            startActivity(
                Intent(this@LoginActivity, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            finish()
        }
        login.setOnClickListener {
            val mMail = binding!!.tEmail.text.toString().trim { it <= ' ' }
            val mPassword = binding!!.tPassword.text.toString()
            val jsonObject = JsonObject()
            jsonObject.addProperty("email", mMail)
            jsonObject.addProperty("password", mPassword)
            jsonObject.addProperty("device_name", Build.MANUFACTURER)
            presenter!!.login(jsonObject)
        }
    }

    override fun showLoading() {
        //
    }

    override fun hideLoading() {
        //
    }

    override fun success(messages: JsonArray?) {
        //
    }

    override fun errors(messages: JsonArray?) {
        val element = messages?.get(0)
        val obj = element?.asJsonObject

        if(obj?.isJsonNull == false) {
            for (key in obj.keySet()) {
                val message = obj[key].asJsonArray
                if(key.equals("email")) {
                    val tErrorEmail: TextView = binding!!.tErrorEmail
                    tErrorEmail.visibility = View.VISIBLE
                    tErrorEmail.text = message?.get(0)?.asString
                }
            }
        }
    }

    override fun onResult(data: Array<LoginData>?) {
        logins = data
        if (logins!!.isNotEmpty()) {
            SharedPrefManager.getInstance(this@LoginActivity)!!.saveData(data!!)
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }
}