package com.example.petdoorz.activity.login

import com.google.gson.JsonArray
import com.example.petdoorz.model.login.LoginData

interface LoginView {
    fun showLoading()
    fun hideLoading()
    fun success(messages: JsonArray?)
    fun errors(messages: JsonArray?)
    fun onResult(data: Array<LoginData>?)
}