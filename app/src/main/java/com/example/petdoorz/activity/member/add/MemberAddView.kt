package com.example.petdoorz.activity.member.add

import com.google.gson.JsonArray

interface MemberAddView {
    fun showLoading()
    fun hideLoading()
    fun messages(messages: JsonArray?)
    fun success(success: Boolean?)
}