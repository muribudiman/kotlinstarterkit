package com.example.petdoorz.activity.member.list

import android.util.Log
import com.example.petdoorz.api.ApiClient.apiClient
import com.example.petdoorz.api.ApiInterface
import com.example.petdoorz.model.member.MemberResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MemberListsPresenter(private val view: MemberListsView) {
    fun getMembers(search: String?, page: Int?) {
        view.showItemLoading()

        //request to server
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getMembers(search, page, 15)
        call!!.enqueue(object : Callback<MemberResponse?> {
            override fun onResponse(
                call: Call<MemberResponse?>,
                response: Response<MemberResponse?>
            ) {
                //Log.d(tag, "onResponse");
                view.hideItemLoading()
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.onResult(response.body()!!.data, response.body()!!.pagination)
                    } else {
                        Log.d(tag, "isSuccess false")
                    }
                } else if (response.code() == 401) {
                } else {
                    Log.d(tag, "body empty")
                }
            }

            override fun onFailure(call: Call<MemberResponse?>, t: Throwable) {
                //Log.d(tag, "onFailure");
                //Log.d(tag, t.getLocalizedMessage());
                //view.hideItemLoading();
            }
        })
    }

    companion object {
        private val tag = MemberListsPresenter::class.java.simpleName
    }
}