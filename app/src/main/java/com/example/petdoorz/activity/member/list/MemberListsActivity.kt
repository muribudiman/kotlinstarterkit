package com.example.petdoorz.activity.member.list

import androidx.appcompat.app.AppCompatActivity
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.activity.result.ActivityResultLauncher
import android.content.Intent
import android.widget.ImageButton
import android.widget.EditText
import android.widget.TextView
import android.os.Bundle
import android.content.pm.ActivityInfo
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import com.example.petdoorz.model.member.MemberAdapter.ItemClickListener
import android.text.TextWatcher
import android.text.Editable
import android.view.View
import androidx.activity.result.ActivityResult
import com.example.petdoorz.model.member.MemberData
import com.example.petdoorz.model.pagination.PaginationData
import com.example.petdoorz.model.member.MemberAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.petdoorz.activity.member.add.MemberAddActivity
import com.example.petdoorz.databinding.ActivityMemberListsBinding
import java.util.*

class MemberListsActivity : AppCompatActivity(), MemberListsView {
    private var binding: ActivityMemberListsBinding? = null
    private var token: String? = null
    private var search: String? = null
    private var shimmerFrameLayout: ShimmerFrameLayout? = null
    private var fab: FloatingActionButton? = null
    private var addForResult: ActivityResultLauncher<Intent>? = null
    private var ibBack: ImageButton? = null
    private var ibPrev: ImageButton? = null
    private var ibNext: ImageButton? = null
    private var etSearch: EditText? = null
    private var tvPageNumber: TextView? = null
    private var currentPage = 0
    private var hasMorePages = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMemberListsBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        fab = binding!!.fab
        shimmerFrameLayout = binding!!.shimmerFrameLayout
        ibBack = binding!!.ibBack
        etSearch = binding!!.etSearch
        ibPrev = binding!!.ibPrev
        ibNext = binding!!.ibNext
        tvPageNumber = binding!!.tvPageNumber
        recyclerView = binding!!.iRecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this)
        presenter = MemberListsPresenter(this)
        getData(1)
        addForResult =
            registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    val intent = result.data
                    val type = intent!!.getStringExtra("type")
                    if (type == "add") {
                        getData(1)
                    } else {
                        getData(1)
                    }
                }
            }
        fab!!.setOnClickListener {
            val intent = Intent(this, MemberAddActivity::class.java)
            intent.putExtra("type", "add")
            addForResult!!.launch(intent)
        }
        itemClickListener = object : ItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                edit(position);
            }
        }
        etSearch!!.setText(search)
        etSearch!!.addTextChangedListener(object : TextWatcher {
            var timer = Timer()
            var DELAY: Long = 1000 // in ms
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(object : TimerTask() {
                        override fun run() {
                            runOnUiThread(object : TimerTask() {
                                override fun run() {
                                    search = s.toString()
                                    getData(1)
                                }
                            })
                        }
                    }, DELAY)
                }
            }
        })
        ibPrev!!.setOnClickListener {
            if (currentPage > 1) {
                getData(currentPage - 1)
            }
        }
        ibNext!!.setOnClickListener {
            if (hasMorePages) {
                getData(currentPage + 1)
            }
        }
        ibBack!!.setOnClickListener { finish() }
    }

    override fun showItemLoading() {
        shimmerFrameLayout!!.visibility = View.VISIBLE
        recyclerView!!.visibility = View.GONE
    }

    override fun hideItemLoading() {
        shimmerFrameLayout!!.visibility = View.GONE
        recyclerView!!.visibility = View.VISIBLE
    }

    override fun onResult(data: List<MemberData?>?, pagination: PaginationData?) {
        currentPage = pagination?.currentPage!!
        hasMorePages = pagination?.hasMorePages!!
        tvPageNumber!!.text = currentPage.toString()
        adapter = MemberAdapter(this, data!! as List<MemberData>, itemClickListener!!)
        adapter!!.notifyDataSetChanged()
        recyclerView!!.adapter = adapter
        items = data as List<MemberData>?
    }

    fun getData(page: Int?) {
        presenter?.getMembers(search, page)
    }

    fun edit(position: Int) {
        val id = items!![position].id
        val name = items!![position].name
        val email = items!![position].email

        val intent = Intent(this, MemberAddActivity::class.java)
        intent.putExtra("type", "edit")
        intent.putExtra("id", id)
        intent.putExtra("name", name)
        intent.putExtra("email", email)
        addForResult!!.launch(intent)
    }

    companion object {
        private val tag = MemberListsActivity::class.java.simpleName
        private var presenter: MemberListsPresenter? = null
        private var adapter: MemberAdapter? = null
        private var itemClickListener: ItemClickListener? = null
        private var items: List<MemberData>? = null
        private var recyclerView: RecyclerView? = null
    }
}