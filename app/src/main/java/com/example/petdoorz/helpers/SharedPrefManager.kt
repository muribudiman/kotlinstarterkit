package com.example.petdoorz.helpers

import android.content.Context
import com.example.petdoorz.model.login.LoginData
import kotlin.jvm.Synchronized

class SharedPrefManager private constructor(private val mCtx: Context) {
    fun saveData(data: Array<LoginData>) {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putInt("id", data?.get(0).id)
        editor.putString("token", data?.get(0).token)
        editor.putString("role", data?.get(0).role)
        editor.putString("type", data?.get(0).type)
        editor.commit()
    }

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt("id", -1) != -1
        }
    val data: LoginData
        get() {
            val sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return LoginData(
                sharedPreferences.getInt("id", -1),
                sharedPreferences.getString("token", null)!!,
                sharedPreferences.getString("role", null)!!,
                sharedPreferences.getString("type", null)!!
            )
        }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private const val SHARED_PREF_NAME = "sinergialam"
        private var mInstance: SharedPrefManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPrefManager? {
            if (mInstance == null) {
                mInstance = SharedPrefManager(mCtx)
            }
            return mInstance
        }
    }
}