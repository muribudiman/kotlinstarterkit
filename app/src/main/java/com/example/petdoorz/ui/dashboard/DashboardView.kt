package com.example.petdoorz.ui.dashboard

import com.example.petdoorz.model.pagination.PaginationData
import com.example.petdoorz.model.vendors.VendorsData

interface DashboardView {
    fun showLoading()
    fun hideLoading()
    fun onResultVendors(data: List<VendorsData>?, pagination: PaginationData?)
}