package com.example.petdoorz.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.petdoorz.R
import com.example.petdoorz.activity.login.LoginActivity
import com.example.petdoorz.activity.member.list.MemberListsActivity
import com.example.petdoorz.databinding.FragmentHomeBinding
import com.example.petdoorz.helpers.HelpersData
import com.example.petdoorz.helpers.SharedPrefManager
import com.example.petdoorz.model.auth.profile.AuthProfileData
import com.example.petdoorz.model.login.LoginData
import com.google.gson.JsonObject
import com.squareup.picasso.Picasso

class HomeFragment : Fragment(), HomeView {

    private var _binding: FragmentHomeBinding? = null

    private var loginForResult: ActivityResultLauncher<Intent>? = null

    private var profile: Array<AuthProfileData>? = null

    private val binding get() = _binding!!

    private var presenter: HomePresenter? = null

    private var helpersData: HelpersData? = null

    // Receiver
    private val getResult =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) {
            if(it.resultCode == Activity.RESULT_OK){
                //val value = it.data?.getStringExtra("input")
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        loginForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { }

        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        val root: View = binding.root
        val ivUser = binding!!.ivUser

        if (SharedPrefManager.getInstance(requireActivity())!!.isLoggedIn) {
            val user: LoginData = SharedPrefManager.getInstance(requireActivity())!!.data

            helpersData = HelpersData(requireActivity())

            presenter = HomePresenter(this)
            presenter!!.getAuthProfile(user.type + " " +user.token)

            ivUser.setOnClickListener{
                val builder = AlertDialog.Builder(
                    requireActivity()
                )
                builder.setTitle(R.string.profile_confirm)
                builder.setMessage(R.string.profile_confirm_text)
                builder.setPositiveButton(R.string.sign_out) { _, _ ->
                    val jsonObject = JsonObject()
                    jsonObject.addProperty("type", "all")
                    presenter!!.postLogout(user!!.token, jsonObject)
                }
                builder.setNegativeButton(R.string.cancel, null)
                builder.show()
            }

        } else {
            val intent = Intent(activity, LoginActivity::class.java)
            loginForResult!!.launch(intent)
            activity?.finish()
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showLoading() {
        //
    }

    override fun hideLoading() {
        //
    }

    override fun logout() {
        helpersData!!.setLogOut()
    }

    override fun onResultAuthProfile(data: Array<AuthProfileData>?) {
        profile = data

        val tvBalance = binding!!.tvBalance
        val tvAuthName = binding!!.tvAuthName
        val tvAuthEmail = binding!!.tvAuthEmail
        val ivProfile = binding!!.ivProfile

        if (profile!!.isNotEmpty()) {
            tvAuthName.text = profile?.get(0)?.name
            tvBalance.text = profile?.get(0)?.balanceFormated
            tvAuthEmail.text = profile?.get(0)?.email

            Picasso.get()
                .load("https://pbs.twimg.com/profile_images/870661657520857088/-e9dDyw8_400x400.jpg")
                .resize(50, 50)
                .centerCrop()
                .error(R.drawable.ic_logo)
                .into(ivProfile)
        }
    }

}