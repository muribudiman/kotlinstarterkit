package com.example.petdoorz.ui.home

import android.util.Log
import com.example.petdoorz.api.ApiClient.apiClient
import com.example.petdoorz.api.ApiInterface
import com.example.petdoorz.model.auth.profile.AuthProfileResponse
import com.example.petdoorz.model.general.GeneralResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(private val view: HomeView) {

    fun getAuthProfile(token: String?) {
        view.showLoading()
        //request to server
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getAuthProfile(token)
        call!!.enqueue(object : Callback<AuthProfileResponse?> {
            override fun onResponse(
                call: Call<AuthProfileResponse?>,
                response: Response<AuthProfileResponse?>
            ) {
                //Log.d(tag, "onResponse");
                view.hideLoading()
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.onResultAuthProfile(response.body()!!.data)
                    } else {
                        Log.d(tag, "isSuccess false")
                    }
                } else if (response.code() == 401) {
                    Log.d(tag, "401")
                } else {
                    Log.d(tag, "body empty")
                }
            }

            override fun onFailure(call: Call<AuthProfileResponse?>, t: Throwable) {
                //Log.d(tag, "onFailure");
                //Log.d(tag, t.getLocalizedMessage());
                //view.hideItemLoading();
            }
        })
    }

    fun postLogout(token: String?, body: JsonObject?) {
        view.showLoading()
        val apiInterface = apiClient!!.create(ApiInterface::class.java)
        val call = apiInterface.logout(token, body)
        call!!.enqueue(object : Callback<GeneralResponse?> {
            override fun onResponse(
                call: Call<GeneralResponse?>,
                response: Response<GeneralResponse?>
            ) {
                view.hideLoading()
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.isSuccess) {
                        Log.d(tag, "onResponse successfully")
                        view.logout()
                    } else {
                        Log.d(tag, "isSuccess false")
                    }
                } else if (response.code() == 401) {
                    view.logout()
                } else {
                    view.logout()
                }
            }

            override fun onFailure(call: Call<GeneralResponse?>, t: Throwable) {
                view.logout()
            }
        })
    }

    companion object {
        private val tag = HomePresenter::class.java.simpleName
    }
}