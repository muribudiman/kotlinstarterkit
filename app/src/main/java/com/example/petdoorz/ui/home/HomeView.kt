package com.example.petdoorz.ui.home

import com.example.petdoorz.model.auth.profile.AuthProfileData
import com.example.petdoorz.model.login.LoginData
import com.example.petdoorz.model.member.MemberData
import com.example.petdoorz.model.pagination.PaginationData

interface HomeView {
    fun showLoading()
    fun hideLoading()
    fun logout()
    fun onResultAuthProfile(data: Array<AuthProfileData>?)
}