package com.example.petdoorz.api

import retrofit2.Retrofit
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
//    private const val URL = "http://192.168.9.246/"  // SMK Mbali wifi
//    private const val URL = "http://192.168.43.235/" // wifi rumah nt5
//    private const val URL = "http://192.168.1.116/" // Respati wifi
//    private const val URL = "http://192.168.1.18/" // TOKONEX wifi
//    private const val URL = "http://192.168.1.104/"
    private const val URL = "http://192.168.1.110/" //RESPATI INFORMATIKA WIFI


    private const val BASE_URL = "api/"
    private var retrofit: Retrofit? = null
    private val httpClient = OkHttpClient.Builder()
        .connectTimeout(240, TimeUnit.SECONDS)
        .readTimeout(240, TimeUnit.SECONDS)
        .writeTimeout(240, TimeUnit.SECONDS)
    val apiClient: Retrofit?
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(URL + BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
            }
            return retrofit
        }
}