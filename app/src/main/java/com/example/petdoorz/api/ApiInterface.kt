package com.example.petdoorz.api

import com.example.petdoorz.model.auth.profile.AuthProfileResponse
import com.example.petdoorz.model.general.GeneralResponse
import com.google.gson.JsonObject
import com.example.petdoorz.model.login.LoginResponse
import com.example.petdoorz.model.member.MemberResponse
import com.example.petdoorz.model.vendors.VendorsResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    //auth
    @Headers("Content-Type: application/json")
    @POST("v1/auth/login")
    fun login(
        @Body body: JsonObject?
    ): Call<LoginResponse?>?

    @Headers("Content-Type: application/json")
    @POST("v1/auth/logout")
    fun logout(
        @Header("Authorization") authToken: String?,
        @Body body: JsonObject?
    ): Call<GeneralResponse?>?

    @GET("v1/auth/profile")
    fun getAuthProfile(
        @Header("Authorization") authToken: String?
    ): Call<AuthProfileResponse?>?
    //end auth

    @GET("v1/members")
    fun getMembers(
        @Query("search") search: String?,
        @Query("page") page: Int?,
        @Query("per_page") per_page: Int?
    ): Call<MemberResponse?>?

    @Headers("Content-Type: application/json")
    @POST("v1/members")
    fun postMember(
        @Body body: JsonObject?
    ): Call<GeneralResponse?>?

    @Headers("Content-Type: application/json")
    @PUT("v1/members/{id}")
    fun putMember(
        @Path("id") id: Int?,
        @Body body: JsonObject?
    ): Call<GeneralResponse?>?

    //vendors
    @GET("v1/users/vendors")
    fun getVendors(
        @Header("Authorization") authToken: String?,
        @Query("search") search: String?,
        @Query("lat") lat: Double?,
        @Query("lng") lng: Double?,
        @Query("page") page: Int?,
        @Query("per_page") per_page: Int?
    ): Call<VendorsResponse?>?
}