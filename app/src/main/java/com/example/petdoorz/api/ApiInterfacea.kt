package com.example.petdoorz.api

import retrofit2.Call
import retrofit2.http.*

interface ApiInterfacea {
//    @Headers("Content-Type: application/json")
//    @POST("v1/auth/member/login")
//    fun login(
//        @Body body: JsonObject?
//    ): Call<LoginResponse?>?
//
//    @Headers("Content-Type: application/json")
//    @POST("v1/auth/member/logout")
//    fun logout(
//        @Header("Authorization") authToken: String?,
//        @Body body: JsonObject?
//    ): Call<GeneralResponse?>?
//
//    @GET("v1/auth/member/profile")
//    fun profile(
//        @Header("Authorization") authToken: String?
//    ): Call<ProfileResponse?>?
//
//    @Headers("Content-Type: application/json")
//    @PUT("v1/auth/member/profile")
//    fun updateMemberProfile(
//        @Header("Authorization") authToken: String?,
//        @Body body: JsonObject?
//    ): Call<GeneralResponse?>?
//
//    @Headers("Content-Type: application/json")
//    @PUT("v1/auth/member/profile/bank")
//    fun updateBankProfile(
//        @Header("Authorization") authToken: String?,
//        @Body body: JsonObject?
//    ): Call<GeneralResponse?>?
//
//    @Multipart
//    @POST("v1/auth/member/profile/image")
//    fun uploadProfilePicture(
//        @Header("Authorization") authToken: String?,
//        @Part photo: MultipartBody.Part?
//    ): Call<GeneralResponse?>?
//
//    @GET("v1/bank_system/slide")
//    fun getSlide(
//        @Query("search") search: String?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<SlideResponse?>?
//
//    @GET("v1/bank_system/trash/item/category")
//    fun getCategory(
//        @Query("search") search: String?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<CategoryResponse?>?
//
//    @GET("v1/bank_system/trash/item")
//    fun getTrashItem(
//        @Query("search") search: String?,
//        @Query("user_id") user_id: Int?,
//        @Query("category_id") category_id: String?,
//        @Query("lat") lat: Double?,
//        @Query("lng") lng: Double?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<TrashResponse?>?
//
//    @GET("v1/bank_system/items")
//    fun getItems(
//        @Query("search") search: String?,
//        @Query("id") id: String?,
//        @Query("user_id") user_id: Int?,
//        @Query("lat") lat: Double?,
//        @Query("lng") lng: Double?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<ItemResponse?>?
//
//    @GET("v1/bank_system/banks")
//    fun getBanks(
//        @Query("search") search: String?,
//        @Query("id") id: String?,
//        @Query("user_id") user_id: Int?,
//        @Query("lat") lat: Double?,
//        @Query("lng") lng: Double?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<BankSampahResponse?>?
//
//    //Bank Account Product Items
//    @GET("v1/banksampah/items")
//    fun getBankSampahTrashProductItem(
//        @Header("Authorization") authToken: String?,
//        @Query("search") search: String?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<BankTrashProductResponse?>?
//
//    // new code for multiple files
//    @Multipart
//    @POST("v1/banksampah/items")
//    fun postBankSampahTrashProductItem(
//        @Header("Authorization") authToken: String?,
//        @Part("name") name: RequestBody?,
//        @Part("price") price: RequestBody?,
//        @Part("unit") unit: RequestBody?,
//        @Part("stock") stock: RequestBody?,
//        @Part("description") description: RequestBody?,
//        @Part photo: MultipartBody.Part?
//    ): Call<GeneralResponse?>?
//
//    @Multipart
//    @POST("v1/banksampah/items/{id}")
//    fun putBankSampahTrashProductItem(
//        @Header("Authorization") authToken: String?,
//        @Path("id") id: String?,
//        @Part("_method") method: RequestBody?,
//        @Part("name") name: RequestBody?,
//        @Part("price") price: RequestBody?,
//        @Part("unit") unit: RequestBody?,
//        @Part("stock") stock: RequestBody?,
//        @Part("description") description: RequestBody?,
//        @Part photo: MultipartBody.Part?
//    ): Call<GeneralResponse?>?
//
//    //Bank Account Trash Items
//    @GET("v1/banksampah/trash/items")
//    fun getBankSampahTrashItem(
//        @Header("Authorization") authToken: String?,
//        @Query("search") search: String?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<BankTrashResponse?>?
//
//    @Headers("Content-Type: application/json")
//    @POST("v1/banksampah/trash/items")
//    fun postBankSampahTrashItem(
//        @Header("Authorization") authToken: String?,
//        @Body body: JsonObject?
//    ): Call<GeneralResponse?>?
//
//    @Headers("Content-Type: application/json")
//    @PUT("v1/banksampah/trash/items/{id}")
//    fun putBankSampahTrashItem(
//        @Header("Authorization") authToken: String?,
//        @Path("id") id: String?,
//        @Body body: JsonObject?
//    ): Call<GeneralResponse?>?
//
//    // Modal
//    @GET("v1/bank_system/trash/item/category")
//    fun getModalCategory(
//        @Query("search") search: String?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<ModalResponse?>?
//
//    @get:GET("v1/general/units")
//    val modalUnit: Call<Any?>?
//
//    @GET("v1/tokonex/products")
//    fun getTn(
//        @Query("search") search: String?,
//        @Query("lat") lat: Double?,
//        @Query("lng") lng: Double?,
//        @Query("page") page: Int?,
//        @Query("per_page") per_page: Int?
//    ): Call<TnResponse?>?
}