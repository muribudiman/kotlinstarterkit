package com.example.petdoorz.model.vendors

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VendorsDetailData(
    @field:Expose @field:SerializedName("id") val id: String,
    @field:Expose @field:SerializedName("company_name") val company_name: String,
    @field:Expose @field:SerializedName("leader_name") val leader_name: String,
    @field:Expose @field:SerializedName("address") val address: String,
    @field:Expose @field:SerializedName("phone") val phone: String,
    @field:Expose @field:SerializedName("mobile") val mobile: String,
    @field:Expose @field:SerializedName("lat") val lat: Double,
    @field:Expose @field:SerializedName("lng") val lng: Double,
    @field:Expose @field:SerializedName("logo") val logo: String
)