package com.example.petdoorz.model.member

import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.view.ViewGroup
import com.example.petdoorz.model.member.MemberAdapter.RecyclerViewAdapter
import android.view.LayoutInflater
import android.view.View
import com.example.petdoorz.R
import android.widget.TextView
import androidx.cardview.widget.CardView

class MemberAdapter(
    private val context: Context,
    private val datas: List<MemberData>,
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<RecyclerViewAdapter>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter {
        val view = LayoutInflater.from(context).inflate(R.layout.item_member, parent, false)
        return RecyclerViewAdapter(view, itemClickListener)
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter, position: Int) {
        val item = datas[position]
        holder.tvName.text = item.name
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    inner class RecyclerViewAdapter(itemView: View, itemClickListener: ItemClickListener) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var tvName: TextView
        var openItem: CardView
        var itemClickListener: ItemClickListener
        override fun onClick(v: View) {
            itemClickListener.onItemClick(v, bindingAdapterPosition)
        }

        init {
            openItem = itemView.findViewById(R.id.openItem)
            tvName = itemView.findViewById(R.id.tvName)
            this.itemClickListener = itemClickListener
            openItem.setOnClickListener(this)
        }
    }

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}