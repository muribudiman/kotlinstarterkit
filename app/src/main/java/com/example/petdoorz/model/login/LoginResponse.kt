package com.example.petdoorz.model.login

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class LoginResponse(
    @field:Expose @field:SerializedName("success") val isSuccess: Boolean,
    @field:Expose @field:SerializedName("message") val message: JsonArray,
    @field:Expose @field:SerializedName("data") val data: Array<LoginData>
)