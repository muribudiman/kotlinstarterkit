package com.example.petdoorz.model.auth.profile

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class AuthProfileData(
    @field:Expose @field:SerializedName("id") val id: Int,
    @field:Expose @field:SerializedName("name") val name: String,
    @field:Expose @field:SerializedName("email") val email: String,
    @field:Expose @field:SerializedName("balanceFormated") val balanceFormated: String,
    @field:Expose @field:SerializedName("profile_photo_url") val profilePhotoUrl: String
)