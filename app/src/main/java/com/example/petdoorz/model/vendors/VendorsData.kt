package com.example.petdoorz.model.vendors

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class VendorsData(
    @field:Expose @field:SerializedName("id") val id: Int,
    @field:Expose @field:SerializedName("name") val name: String,
    @field:Expose @field:SerializedName("email") val email: String,
    @field:Expose @field:SerializedName("distance") val distance: Double,
    @field:Expose @field:SerializedName("distanceString") val distanceString: String,
    @field:Expose @field:SerializedName("logoUrl") val logoUrl: String,
    @field:Expose @field:SerializedName("user_detail") val user_detail: VendorsDetailData
)