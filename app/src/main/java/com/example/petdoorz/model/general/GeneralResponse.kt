package com.example.petdoorz.model.general

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class GeneralResponse(
    @field:Expose @field:SerializedName("success") val isSuccess: Boolean,
    @field:Expose @field:SerializedName("message") val message: JsonArray
)