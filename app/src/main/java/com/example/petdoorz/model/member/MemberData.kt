package com.example.petdoorz.model.member

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class MemberData(
    @field:Expose @field:SerializedName("id") val id: Int,
    @field:Expose @field:SerializedName("name") val name: String,
    @field:Expose @field:SerializedName("email") val email: String,
    @field:Expose @field:SerializedName("profile_photo_url") val profile_photo_url: String
)