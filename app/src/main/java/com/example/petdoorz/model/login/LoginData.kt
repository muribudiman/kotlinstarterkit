package com.example.petdoorz.model.login

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class LoginData(
    @field:Expose @field:SerializedName("id") val id: Int,
    @field:Expose @field:SerializedName("token") val token: String,
    @field:Expose @field:SerializedName("role") val role: String,
    @field:Expose @field:SerializedName("type") val type: String
)