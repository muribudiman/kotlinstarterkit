package com.example.petdoorz.model.pagination

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class PaginationData(
    @field:Expose @field:SerializedName("total") val total: Int,
    @field:Expose @field:SerializedName("currentPage") val currentPage: Int,
    @field:Expose @field:SerializedName("lastPage") val lastPage: Int,
    @field:Expose @field:SerializedName("hasMorePages") val hasMorePages: Boolean,
    @field:Expose @field:SerializedName("perPage") val perPage: Int,
    @field:Expose @field:SerializedName("lastItem") val lastItem: Int
)