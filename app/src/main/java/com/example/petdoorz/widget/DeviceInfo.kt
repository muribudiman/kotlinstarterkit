package com.example.petdoorz.widget

import java.io.Serializable

class DeviceInfo : Serializable {
    @JvmField
    var device: String? = null
    @JvmField
    var os_version: String? = null
    @JvmField
    var app_version: String? = null
    @JvmField
    var serial: String? = null
    var regid: String? = null
}